# Scale Pi Hat


## Purpose

Connect the Hat to the BPI-P2 Zero and create the extension connections for the Scale project.


## Board

Part 73-290

![](img/asembledboard.png)

![](img/bottomview.png)

## BOM

| Id | Designator  | Package                                           | Quantity | Designation       |
|----|-------------|---------------------------------------------------|----------|-------------------|
| 1  | H2,H1       | MountingHole_2.7mm_Pad_TopBottom                  | 2        | h                 |
| 2  | C2          | C_1206_3216Metric_Pad1.33x1.80mm_HandSolder       | 1        | 100nF             |
| 3  | J1          | PinHeader_1x05_P2.54mm_Vertical                   | 1        | Rotary            |
| 4  | BZ1         | MagneticBuzzer_CUI_CST-931RP-A                    | 1        | Buzzer            |
| 5  | C1          | CP_Radial_D10.0mm_P2.50mm                         | 1        | 22micro           |
| 6  | C3,C4       | CP_Radial_D10.0mm_P2.50mm                         | 2        | 47micro           |
| 7  | CON1        | PinSocket_2x01_P2.54mm_Vertical                   | 1        | USB2_BPI_P2_Zero  |
| 8  | CON2        | PinSocket_2x20_P2.54mm_Vertical                   | 1        | GPIO BPI P2 Zero  |
| 9  | CON3        | PinSocket_2x01_P2.54mm_Vertical                   | 1        | BCP               |
| 10 | CON4        | PinSocket_2x01_P2.54mm_Vertical                   | 1        | WP                |
| 11 | CON6        | PinSocket_2x01_P2.54mm_Vertical                   | 1        | BL                |
| 12 | D1          | LED_D5.0mm                                        | 1        | heartbeat         |
| 13 | D2          | LED_D5.0mm                                        | 1        | POWER             |
| 14 | D3          | D_SMA-SMB_Universal_Handsoldering                 | 1        |  SSA33L           |
| 15 | J2          | PinHeader_1x04_P2.54mm_Vertical                   | 1        | RS232 TTL         |
| 16 | J3          | PinHeader_1x04_P2.54mm_Vertical                   | 1        | OLED_IIC          |
| 17 | J4          | PinHeader_2x04_P2.54mm_Vertical                   | 1        | USB/RS232         |
| 18 | J5          | Hirose_FH12-12S-0.5SH_1x12-1MP_P0.50mm_Horizontal | 1        | Barcode Reader    |
| 19 | J6          | PinHeader_1x06_P2.54mm_Vertical                   | 1        | Finger_Print      |
| 20 | J7          | PinHeader_1x07_P2.54mm_Vertical                   | 1        | OLED_SPI          |
| 21 | J8          | PinHeader_1x08_P2.54mm_Vertical                   | 1        | RFID_SPI          |
| 22 | J9          | PinHeader_2x04_P2.54mm_Vertical                   | 1        | Expansion         |
| 23 | L1          | L_TDK_VLF10040                                    | 1        | VLF10040T-100M3R8 |
| 24 | Q1          | TO-92_Inline                                      | 1        | BC547             |
| 25 | R1,R3,R4,R5 | R_1206_3216Metric                                 | 4        | 330               |
| 26 | R2          | R_1206_3216Metric                                 | 1        | 1K                |
| 27 | U1          | SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm           | 1        | ADP2302ARDZ-3.3   |
| 28 | U2          | MSOP-8_3x3mm_P0.65mm                              | 1        | 24LC256           |

## Schematic

![](img/schematic_ref.png)

## CON2

If we consider to also do fingerprinting, we need an extra uart and we're out:

|UART| Function|
|---|---|
| UART0 | debug console |
| UART1 | not available, goes to Bluetooth IC |
| UART2 | TTL rs232 for scale |
|UART3| Barcode |

So we can take USB2 on CON2 and switch to Barcode Reader with **j4** *USB/RS232* jumper block, and thus using UART3 for Finger PrintSensor while using USB2 for the Barcode Reader.

