EESchema Schematic File Version 4
LIBS:my scale-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "BPI-P2 zero M HAT for Scale"
Date "2021-01-03"
Rev "-"
Comp "Gioxa Ltd"
Comment1 "Embeded Linux "
Comment2 "PCB part 73-290"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L my-scale-rescue:BPI-GPIO-CON-Connector_Generic CON2
U 1 1 5FF01CEC
P 1950 2500
F 0 "CON2" H 1950 1350 50  0000 C CNN
F 1 "GPIO BPI P2 Zero" H 2150 1250 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 1950 2500 50  0001 C CNN
F 3 "~" H 1950 2500 50  0001 C CNN
	1    1950 2500
	1    0    0    -1  
$EndComp
Text GLabel 2750 1900 2    50   Output ~ 0
UART3-TX
Text GLabel 1400 1700 0    50   BiDi ~ 0
OLED-SDA
Wire Wire Line
	1400 1700 1750 1700
Text GLabel 1400 1800 0    50   Output ~ 0
OLED-SCK
Wire Wire Line
	1400 1800 1750 1800
Wire Wire Line
	2750 1900 2250 1900
Text GLabel 2750 2000 2    50   Input ~ 0
UART3-RX
Wire Wire Line
	2250 2000 2750 2000
$Comp
L my-scale-rescue:+3.3V-power #PWR0101
U 1 1 5FF08AB8
P 1450 1600
F 0 "#PWR0101" H 1450 1450 50  0001 C CNN
F 1 "+3.3V" V 1465 1728 50  0000 L CNN
F 2 "" H 1450 1600 50  0001 C CNN
F 3 "" H 1450 1600 50  0001 C CNN
	1    1450 1600
	0    -1   -1   0   
$EndComp
$Comp
L my-scale-rescue:+5V-power #PWR0102
U 1 1 5FF093DF
P 2450 1600
F 0 "#PWR0102" H 2450 1450 50  0001 C CNN
F 1 "+5V" V 2465 1728 50  0000 L CNN
F 2 "" H 2450 1600 50  0001 C CNN
F 3 "" H 2450 1600 50  0001 C CNN
	1    2450 1600
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR0103
U 1 1 5FF09C55
P 2800 1800
F 0 "#PWR0103" H 2800 1550 50  0001 C CNN
F 1 "GND" V 2805 1672 50  0000 R CNN
F 2 "" H 2800 1800 50  0001 C CNN
F 3 "" H 2800 1800 50  0001 C CNN
	1    2800 1800
	0    -1   1    0   
$EndComp
$Comp
L my-scale-rescue:+5V-power #PWR0104
U 1 1 5FF0A70F
P 2450 1700
F 0 "#PWR0104" H 2450 1550 50  0001 C CNN
F 1 "+5V" V 2465 1828 50  0000 L CNN
F 2 "" H 2450 1700 50  0001 C CNN
F 3 "" H 2450 1700 50  0001 C CNN
	1    2450 1700
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR0105
U 1 1 5FF0C246
P 2500 2500
F 0 "#PWR0105" H 2500 2250 50  0001 C CNN
F 1 "GND" V 2505 2372 50  0000 R CNN
F 2 "" H 2500 2500 50  0001 C CNN
F 3 "" H 2500 2500 50  0001 C CNN
	1    2500 2500
	0    -1   1    0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR0106
U 1 1 5FF0CC74
P 2800 3200
F 0 "#PWR0106" H 2800 2950 50  0001 C CNN
F 1 "GND" V 2805 3072 50  0000 R CNN
F 2 "" H 2800 3200 50  0001 C CNN
F 3 "" H 2800 3200 50  0001 C CNN
	1    2800 3200
	0    -1   1    0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR0108
U 1 1 5FF0DA7F
P 1450 2000
F 0 "#PWR0108" H 1450 1750 50  0001 C CNN
F 1 "GND" V 1455 1872 50  0000 R CNN
F 2 "" H 1450 2000 50  0001 C CNN
F 3 "" H 1450 2000 50  0001 C CNN
	1    1450 2000
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR0109
U 1 1 5FF0E128
P 1450 3500
F 0 "#PWR0109" H 1450 3250 50  0001 C CNN
F 1 "GND" V 1455 3372 50  0000 R CNN
F 2 "" H 1450 3500 50  0001 C CNN
F 3 "" H 1450 3500 50  0001 C CNN
	1    1450 3500
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR0110
U 1 1 5FF0EA07
P 1000 2800
F 0 "#PWR0110" H 1000 2550 50  0001 C CNN
F 1 "GND" V 1005 2672 50  0000 R CNN
F 2 "" H 1000 2800 50  0001 C CNN
F 3 "" H 1000 2800 50  0001 C CNN
	1    1000 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	1450 2000 1750 2000
Wire Wire Line
	1000 2800 1750 2800
Wire Wire Line
	1450 3500 1750 3500
Wire Wire Line
	2250 3200 2800 3200
Wire Wire Line
	2250 3000 2800 3000
Wire Wire Line
	2250 2500 2500 2500
Wire Wire Line
	2250 1800 2800 1800
Wire Wire Line
	2250 1700 2450 1700
Wire Wire Line
	2250 1600 2450 1600
Wire Wire Line
	1450 1600 1750 1600
Text GLabel 1250 2100 0    50   Input ~ 0
UART2-RX
Text GLabel 1250 2200 0    50   Output ~ 0
UART2-TX
Wire Wire Line
	1250 2100 1750 2100
Wire Wire Line
	1250 2200 1750 2200
$Comp
L my-scale-rescue:GND-power #PWR0112
U 1 1 5FF155B5
P 2800 2200
F 0 "#PWR0112" H 2800 1950 50  0001 C CNN
F 1 "GND" V 2805 2072 50  0000 R CNN
F 2 "" H 2800 2200 50  0001 C CNN
F 3 "" H 2800 2200 50  0001 C CNN
	1    2800 2200
	0    -1   1    0   
$EndComp
Wire Wire Line
	2250 2200 2800 2200
Text GLabel 1200 2900 0    50   BiDi ~ 0
ID-SDA
Wire Wire Line
	1200 2900 1750 2900
Text GLabel 2850 3400 2    50   Input ~ 0
Rotary-A
Text GLabel 2850 3500 2    50   Input ~ 0
Rotary-B
Wire Wire Line
	2250 3400 2850 3400
Wire Wire Line
	2250 3500 2850 3500
Text GLabel 1550 3400 0    50   Output ~ 0
SPDIF-OUT
Wire Wire Line
	1550 3400 1750 3400
Text GLabel 1400 2500 0    50   Output ~ 0
SPI_MOSI
Text GLabel 1400 2600 0    50   Input ~ 0
SPI_MISO
Text GLabel 1400 2700 0    50   Output ~ 0
SPI_CLK
Text GLabel 2900 2700 2    50   Output ~ 0
SPI_CE0
Text GLabel 2900 2800 2    50   Output ~ 0
SPI_CE1
Wire Wire Line
	1400 2500 1750 2500
Wire Wire Line
	1400 2600 1750 2600
Wire Wire Line
	1400 2700 1750 2700
Wire Wire Line
	2250 2700 2900 2700
Wire Wire Line
	2250 2800 2900 2800
Text GLabel 2850 3300 2    50   Input ~ 0
Rotary-SW
Wire Wire Line
	2850 3300 2250 3300
Text GLabel 1650 3000 0    50   BiDi Italic 0
PA7-INT7
Text GLabel 1650 3100 0    50   BiDi Italic 0
PA8-INT8
Text GLabel 1650 3200 0    50   BiDi Italic 0
PA9-INT9
Text GLabel 1650 3300 0    50   BiDi Italic 0
PA10-INT10
$Comp
L my-scale-rescue:Conn_02x01-Connector_Generic CON1
U 1 1 5FF2B2AB
P 1950 4050
F 0 "CON1" H 2000 4267 50  0000 C CNN
F 1 "USB2_BPI_P2_Zero" H 2250 4200 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x01_P2.54mm_Vertical" H 1950 4050 50  0001 C CNN
F 3 "~" H 1950 4050 50  0001 C CNN
	1    1950 4050
	1    0    0    -1  
$EndComp
Text GLabel 1600 4050 0    50   BiDi Italic 0
USBD2+
Text GLabel 2400 4050 2    50   BiDi Italic 0
USBD2-
Wire Wire Line
	2250 4050 2400 4050
Wire Wire Line
	1600 4050 1750 4050
$Comp
L my-scale-rescue:Conn_01x05-Connector_Generic J1
U 1 1 5FF323D5
P 8950 1250
F 0 "J1" H 9030 1292 50  0000 L CNN
F 1 "Rotary" H 9030 1201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 8950 1250 50  0001 C CNN
F 3 "~" H 8950 1250 50  0001 C CNN
	1    8950 1250
	1    0    0    -1  
$EndComp
Text GLabel 8550 1250 0    50   Input ~ 0
Rotary-SW
Text GLabel 8550 1350 0    50   Input ~ 0
Rotary-A
Text GLabel 8550 1450 0    50   Input ~ 0
Rotary-B
$Comp
L my-scale-rescue:GND-power #PWR013
U 1 1 5FF37008
P 8450 1050
F 0 "#PWR013" H 8450 800 50  0001 C CNN
F 1 "GND" V 8455 922 50  0000 R CNN
F 2 "" H 8450 1050 50  0001 C CNN
F 3 "" H 8450 1050 50  0001 C CNN
	1    8450 1050
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR014
U 1 1 5FF37880
P 8350 1150
F 0 "#PWR014" H 8350 1000 50  0001 C CNN
F 1 "+3.3V" V 8365 1278 50  0000 L CNN
F 2 "" H 8350 1150 50  0001 C CNN
F 3 "" H 8350 1150 50  0001 C CNN
	1    8350 1150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8550 1250 8750 1250
Wire Wire Line
	8550 1350 8750 1350
Wire Wire Line
	8550 1450 8750 1450
Text Notes 9300 900  2    98   Italic 20
Rotary + switch
Wire Notes Line width 12
	9600 700  9600 1650
Wire Notes Line width 12
	9600 1650 8000 1650
Wire Notes Line width 12
	8000 1650 8000 700 
Wire Notes Line width 12
	8000 700  9600 700 
$Comp
L my-scale-rescue:Conn_01x04-Connector_Generic J2
U 1 1 5FF47DDC
P 9050 2400
F 0 "J2" H 9130 2392 50  0000 L CNN
F 1 "RS232 TTL" H 9130 2301 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9050 2400 50  0001 C CNN
F 3 "~" H 9050 2400 50  0001 C CNN
	1    9050 2400
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR015
U 1 1 5FF49B79
P 8550 2300
F 0 "#PWR015" H 8550 2050 50  0001 C CNN
F 1 "GND" V 8555 2172 50  0000 R CNN
F 2 "" H 8550 2300 50  0001 C CNN
F 3 "" H 8550 2300 50  0001 C CNN
	1    8550 2300
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR016
U 1 1 5FF49B83
P 8600 2400
F 0 "#PWR016" H 8600 2250 50  0001 C CNN
F 1 "+3.3V" V 8615 2528 50  0000 L CNN
F 2 "" H 8600 2400 50  0001 C CNN
F 3 "" H 8600 2400 50  0001 C CNN
	1    8600 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8550 2300 8850 2300
Wire Wire Line
	8600 2400 8850 2400
Text GLabel 8600 2500 0    50   Output ~ 0
UART2-TX
Text GLabel 8600 2600 0    50   Input ~ 0
UART2-RX
Wire Wire Line
	8600 2500 8850 2500
Wire Wire Line
	8600 2600 8850 2600
Text GLabel 6250 1150 0    50   BiDi ~ 0
OLED-SDA
Wire Wire Line
	6250 1150 6600 1150
Text GLabel 6250 1250 0    50   Output ~ 0
OLED-SCK
Wire Wire Line
	6250 1250 6600 1250
$Comp
L my-scale-rescue:+3.3V-power #PWR010
U 1 1 5FF64F76
P 6300 1350
F 0 "#PWR010" H 6300 1200 50  0001 C CNN
F 1 "+3.3V" V 6315 1478 50  0000 L CNN
F 2 "" H 6300 1350 50  0001 C CNN
F 3 "" H 6300 1350 50  0001 C CNN
	1    6300 1350
	0    -1   -1   0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR012
U 1 1 5FF64F80
P 6400 2550
F 0 "#PWR012" H 6400 2300 50  0001 C CNN
F 1 "GND" V 6405 2422 50  0000 R CNN
F 2 "" H 6400 2550 50  0001 C CNN
F 3 "" H 6400 2550 50  0001 C CNN
	1    6400 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2550 6700 2550
Wire Wire Line
	6300 1350 6600 1350
$Comp
L my-scale-rescue:Conn_01x04-Connector_Generic J3
U 1 1 5FF6B254
P 6800 1350
F 0 "J3" H 6718 925 50  0000 C CNN
F 1 "OLED_IIC" H 6718 1016 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6800 1350 50  0001 C CNN
F 3 "~" H 6800 1350 50  0001 C CNN
	1    6800 1350
	1    0    0    1   
$EndComp
Text Notes 9350 2150 2    98   Italic 20
Scale RS232
Wire Notes Line width 12
	9600 1950 9600 2800
Wire Notes Line width 12
	9600 2800 7950 2800
Wire Notes Line width 12
	7950 2800 7950 1950
Wire Notes Line width 12
	7950 1950 9600 1950
Wire Notes Line width 12
	5450 1650 7250 1650
Wire Notes Line width 12
	7250 1650 7250 600 
Wire Notes Line width 12
	7250 600  5450 600 
Text Notes 6500 850  2    98   Italic 20
OLED IIC
$Comp
L my-scale-rescue:Conn_01x12-Connector_Generic J5
U 1 1 5FF88AF6
P 6900 2850
F 0 "J5" H 6980 2842 50  0000 L CNN
F 1 "Barcode Reader" V 7150 2450 50  0000 L CNN
F 2 "Connector_FFC-FPC:Hirose_FH12-12S-0.5SH_1x12-1MP_P0.50mm_Horizontal" H 6900 2850 50  0001 C CNN
F 3 "~" H 6900 2850 50  0001 C CNN
	1    6900 2850
	1    0    0    -1  
$EndComp
Wire Notes Line width 12
	5450 600  5450 1650
$Comp
L my-scale-rescue:GND-power #PWR011
U 1 1 5FF8E46C
P 6300 1450
F 0 "#PWR011" H 6300 1200 50  0001 C CNN
F 1 "GND" V 6305 1322 50  0000 R CNN
F 2 "" H 6300 1450 50  0001 C CNN
F 3 "" H 6300 1450 50  0001 C CNN
	1    6300 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	6300 1450 6600 1450
$Comp
L my-scale-rescue:GND-power #PWR09
U 1 1 5FF96FD1
P 4600 1650
F 0 "#PWR09" H 4600 1400 50  0001 C CNN
F 1 "GND" V 4605 1522 50  0000 R CNN
F 2 "" H 4600 1650 50  0001 C CNN
F 3 "" H 4600 1650 50  0001 C CNN
	1    4600 1650
	1    0    0    -1  
$EndComp
Text GLabel 5900 2750 0    50   Output ~ 0
UART3-RX
Text GLabel 5900 2650 0    50   Input ~ 0
UART3-TX
$Comp
L my-scale-rescue:+3.3V-power #PWR08
U 1 1 5FFA32FC
P 4600 1100
F 0 "#PWR08" H 4600 950 50  0001 C CNN
F 1 "+3.3V" H 4615 1273 50  0000 C CNN
F 2 "" H 4600 1100 50  0001 C CNN
F 3 "" H 4600 1100 50  0001 C CNN
	1    4600 1100
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:24LC256-Memory_EEPROM U2
U 1 1 5FFA9CF8
P 8600 3700
F 0 "U2" H 8250 4100 50  0000 C CNN
F 1 "24LC256" H 8200 4000 50  0000 C CNN
F 2 "Package_SO:MSOP-8_3x3mm_P0.65mm" H 8600 3700 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/21203m.pdf" H 8600 3700 50  0001 C CNN
	1    8600 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 2900 2900 2900
Text GLabel 2900 2900 2    50   Output ~ 0
ID-SCK
$Comp
L my-scale-rescue:GND-power #PWR0107
U 1 1 5FF0D625
P 2800 3000
F 0 "#PWR0107" H 2800 2750 50  0001 C CNN
F 1 "GND" V 2805 2872 50  0000 R CNN
F 2 "" H 2800 3000 50  0001 C CNN
F 3 "" H 2800 3000 50  0001 C CNN
	1    2800 3000
	0    -1   1    0   
$EndComp
Text GLabel 9150 3700 2    50   Output ~ 0
ID-SCK
$Comp
L my-scale-rescue:GND-power #PWR018
U 1 1 5FFB1700
P 8600 4250
F 0 "#PWR018" H 8600 4000 50  0001 C CNN
F 1 "GND" V 8605 4122 50  0000 R CNN
F 2 "" H 8600 4250 50  0001 C CNN
F 3 "" H 8600 4250 50  0001 C CNN
	1    8600 4250
	1    0    0    -1  
$EndComp
Text GLabel 9150 3600 2    50   BiDi ~ 0
ID-SDA
$Comp
L my-scale-rescue:Conn_02x01-Connector_Generic CON3
U 1 1 5FFB947E
P 4000 6150
F 0 "CON3" H 4050 6367 50  0000 C CNN
F 1 "BCP" H 4050 6276 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x01_P2.54mm_Vertical" H 4000 6150 50  0001 C CNN
F 3 "~" H 4000 6150 50  0001 C CNN
	1    4000 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 3600 9150 3600
Wire Wire Line
	9000 3700 9150 3700
Wire Wire Line
	8600 4150 8600 4250
Wire Wire Line
	8600 4000 8600 4150
Connection ~ 8600 4150
Wire Wire Line
	8200 3600 8200 3700
Wire Wire Line
	8200 4150 8600 4150
Connection ~ 8200 3700
Wire Wire Line
	8200 3700 8200 3800
Connection ~ 8200 3800
Wire Wire Line
	8200 3800 8200 4150
$Comp
L my-scale-rescue:+3.3V-power #PWR07
U 1 1 5FFCD86B
P 3650 5900
F 0 "#PWR07" H 3650 5750 50  0001 C CNN
F 1 "+3.3V" H 3665 6073 50  0000 C CNN
F 2 "" H 3650 5900 50  0001 C CNN
F 3 "" H 3650 5900 50  0001 C CNN
	1    3650 5900
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:Buzzer-Device BZ1
U 1 1 5FFD49BE
P 4600 3500
F 0 "BZ1" H 4752 3529 50  0000 L CNN
F 1 "Buzzer" H 4752 3438 50  0000 L CNN
F 2 "Buzzer_Beeper:MagneticBuzzer_CUI_CST-931RP-A" V 4575 3600 50  0001 C CNN
F 3 "~" V 4575 3600 50  0001 C CNN
	1    4600 3500
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR02
U 1 1 5FFD8A81
P 1550 2400
F 0 "#PWR02" H 1550 2250 50  0001 C CNN
F 1 "+3.3V" V 1565 2528 50  0000 L CNN
F 2 "" H 1550 2400 50  0001 C CNN
F 3 "" H 1550 2400 50  0001 C CNN
	1    1550 2400
	0    -1   -1   0   
$EndComp
Text GLabel 1250 2300 0    50   Output ~ 0
Barcode-TRIG
Wire Wire Line
	1250 2300 1750 2300
Text GLabel 4050 3850 1    50   Input ~ 0
PWM1
$Comp
L my-scale-rescue:+5V-power #PWR01
U 1 1 5FFDF655
P 1050 5700
F 0 "#PWR01" H 1050 5550 50  0001 C CNN
F 1 "+5V" H 1065 5873 50  0000 C CNN
F 2 "" H 1050 5700 50  0001 C CNN
F 3 "" H 1050 5700 50  0001 C CNN
	1    1050 5700
	1    0    0    -1  
$EndComp
Text GLabel 6600 3450 0    50   Output ~ 0
Barcode-TRIG
Text GLabel 1900 6600 0    50   Input ~ 0
Barcode-PWR
$Comp
L my-scale-rescue:ADP2302ARDZ-3.3-Regulator_Switching U1
U 1 1 5FFE4977
P 2450 6600
F 0 "U1" H 2450 7067 50  0000 C CNN
F 1 "ADP2302ARDZ-3.3" H 2450 6976 50  0000 C CNN
F 2 "Package_SO:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm" H 2600 6250 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADP2302_2303.pdf" H 2250 7200 50  0001 C CNN
	1    2450 6600
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:INDUCTOR-pspice L1
U 1 1 5FFFE181
P 4000 6600
F 0 "L1" H 4000 6815 50  0000 C CNN
F 1 "VLF10040T-100M3R8" H 4000 6724 50  0000 C CNN
F 2 "Inductor_SMD:L_TDK_VLF10040" H 4000 6600 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/TDK%20PDFs/VLF10040.pdf" H 4000 6600 50  0001 C CNN
	1    4000 6600
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:C-Device C2
U 1 1 5FFFF668
P 3250 6400
F 0 "C2" V 3502 6400 50  0000 C CNN
F 1 "100nF" V 3411 6400 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 3288 6250 50  0001 C CNN
F 3 "~" H 3250 6400 50  0001 C CNN
	1    3250 6400
	0    -1   -1   0   
$EndComp
$Comp
L my-scale-rescue:CP-Device C3
U 1 1 60002F62
P 4300 7000
F 0 "C3" H 4418 7046 50  0000 L CNN
F 1 "47micro" H 4418 6955 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P2.50mm" H 4338 6850 50  0001 C CNN
F 3 "~" H 4300 7000 50  0001 C CNN
F 4 "3V3 Barcode" H 4300 7000 50  0001 C CNN "Function"
	1    4300 7000
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR03
U 1 1 60009A1C
P 2550 7400
F 0 "#PWR03" H 2550 7150 50  0001 C CNN
F 1 "GND" V 2555 7272 50  0000 R CNN
F 2 "" H 2550 7400 50  0001 C CNN
F 3 "" H 2550 7400 50  0001 C CNN
	1    2550 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 7150 4300 7250
Wire Wire Line
	4300 7250 3600 7250
Wire Wire Line
	2550 7250 2550 7400
Wire Wire Line
	2450 7000 2550 7000
Wire Wire Line
	2550 7250 2550 7000
Connection ~ 2550 7250
Connection ~ 2550 7000
Wire Wire Line
	2950 6800 4300 6800
Wire Wire Line
	4300 6800 4300 6850
Wire Wire Line
	4300 6600 4300 6800
Connection ~ 4300 6800
Wire Wire Line
	2950 6600 3600 6600
Wire Wire Line
	2950 6400 3100 6400
Wire Wire Line
	3400 6400 3600 6400
Wire Wire Line
	3600 6400 3600 6600
Connection ~ 3600 6600
Wire Wire Line
	3600 6600 3750 6600
$Comp
L my-scale-rescue:DIODE-pspice D3
U 1 1 6002D66B
P 3600 6950
F 0 "D3" V 3646 6822 50  0000 R CNN
F 1 " SSA33L" V 3555 6822 50  0000 R CNN
F 2 "Diode_SMD:D_SMA-SMB_Universal_Handsoldering" H 3600 6950 50  0001 C CNN
F 3 "https://www.vishay.com/docs/88883/ssa33l.pdf" H 3600 6950 50  0001 C CNN
	1    3600 6950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3600 6600 3600 6750
Wire Wire Line
	3600 7150 3600 7250
Connection ~ 3600 7250
Wire Wire Line
	3600 7250 2550 7250
Wire Wire Line
	4250 6600 4300 6600
$Comp
L my-scale-rescue:CP-Device C1
U 1 1 600382B0
P 950 6700
F 0 "C1" H 1068 6746 50  0000 L CNN
F 1 "22micro" H 1068 6655 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P2.50mm" H 988 6550 50  0001 C CNN
F 3 "~" H 950 6700 50  0001 C CNN
	1    950  6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 7250 950  7250
Wire Wire Line
	950  7250 950  6850
$Comp
L my-scale-rescue:LED-Device D2
U 1 1 6004B342
P 1250 6000
F 0 "D2" V 1289 5882 50  0000 R CNN
F 1 "POWER" V 1198 5882 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 1250 6000 50  0001 C CNN
F 3 "~" H 1250 6000 50  0001 C CNN
	1    1250 6000
	0    -1   -1   0   
$EndComp
$Comp
L my-scale-rescue:R-Device R1
U 1 1 60050CFB
P 1250 6400
F 0 "R1" H 1320 6446 50  0000 L CNN
F 1 "330" H 1320 6355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1180 6400 50  0001 C CNN
F 3 "~" H 1250 6400 50  0001 C CNN
	1    1250 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6600 1950 6600
Wire Wire Line
	1250 5750 1250 5850
Wire Wire Line
	950  5750 950  6550
Wire Wire Line
	1250 6150 1250 6250
Wire Wire Line
	1250 6550 1250 6800
Wire Wire Line
	1250 6800 1950 6800
Wire Wire Line
	1250 5750 1750 5750
Wire Wire Line
	1750 5750 1750 6400
Wire Wire Line
	1750 6400 1950 6400
Connection ~ 1250 5750
$Comp
L my-scale-rescue:CP-Device C4
U 1 1 600A18AC
P 4600 1350
F 0 "C4" H 4718 1396 50  0000 L CNN
F 1 "47micro" H 4718 1305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P2.50mm" H 4638 1200 50  0001 C CNN
F 3 "~" H 4600 1350 50  0001 C CNN
	1    4600 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 1100 4600 1200
Wire Wire Line
	4600 1500 4600 1650
Text GLabel 4450 6150 2    50   UnSpc ~ 0
3V3_Barcode
Wire Wire Line
	4450 6150 4300 6150
Wire Wire Line
	4300 6150 4300 6600
Connection ~ 4300 6600
Text GLabel 6600 2450 0    50   UnSpc ~ 0
3V3_Barcode
Text GLabel 5900 2850 0    50   BiDi Italic 0
USBD2-
Text GLabel 5900 2950 0    50   BiDi Italic 0
USBD2+
$Comp
L my-scale-rescue:Conn_02x04_Odd_Even-Connector_Generic J4
U 1 1 600D771A
P 6200 2750
F 0 "J4" H 6250 2600 50  0000 C CNN
F 1 "USB/RS232" H 6200 2450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 6200 2750 50  0001 C CNN
F 3 "~" H 6200 2750 50  0001 C CNN
	1    6200 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2650 6700 2650
Wire Wire Line
	6500 2750 6700 2750
Wire Wire Line
	6500 2850 6700 2850
Wire Wire Line
	6500 2950 6700 2950
Wire Wire Line
	5900 2950 6000 2950
Wire Wire Line
	5900 2850 6000 2850
Wire Wire Line
	5900 2750 6000 2750
Wire Wire Line
	5900 2650 6000 2650
Wire Wire Line
	6000 2650 6500 2650
Connection ~ 6000 2650
Connection ~ 6500 2650
Wire Wire Line
	6000 2750 6500 2750
Connection ~ 6000 2750
Connection ~ 6500 2750
Wire Wire Line
	6600 3450 6700 3450
$Comp
L my-scale-rescue:BC547-Transistor_BJT Q1
U 1 1 60117F64
P 4300 4350
F 0 "Q1" H 4491 4396 50  0000 L CNN
F 1 "BC547" H 4491 4305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4500 4275 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC550-D.pdf" H 4300 4350 50  0001 L CNN
	1    4300 4350
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR06
U 1 1 60118EBA
P 4400 4650
F 0 "#PWR06" H 4400 4400 50  0001 C CNN
F 1 "GND" V 4405 4522 50  0000 R CNN
F 2 "" H 4400 4650 50  0001 C CNN
F 3 "" H 4400 4650 50  0001 C CNN
	1    4400 4650
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:R-Device R2
U 1 1 60119DBD
P 4050 4100
F 0 "R2" V 4100 3900 50  0000 C CNN
F 1 "1K" V 4050 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 3980 4100 50  0001 C CNN
F 3 "~" H 4050 4100 50  0001 C CNN
	1    4050 4100
	-1   0    0    1   
$EndComp
$Comp
L my-scale-rescue:R-Device R4
U 1 1 6011A5F9
P 4400 3850
F 0 "R4" H 4470 3896 50  0000 L CNN
F 1 "330" H 4470 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4330 3850 50  0001 C CNN
F 3 "~" H 4400 3850 50  0001 C CNN
	1    4400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 3100 2900 3100
Wire Wire Line
	4400 4550 4400 4650
Wire Wire Line
	4400 4150 4400 4000
$Comp
L my-scale-rescue:+5V-power #PWR05
U 1 1 601316DB
P 4400 3200
F 0 "#PWR05" H 4400 3050 50  0001 C CNN
F 1 "+5V" H 4415 3373 50  0000 C CNN
F 2 "" H 4400 3200 50  0001 C CNN
F 3 "" H 4400 3200 50  0001 C CNN
	1    4400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3200 4400 3400
Wire Wire Line
	4400 3400 4500 3400
Wire Wire Line
	4500 3600 4400 3600
Wire Wire Line
	4400 3600 4400 3700
$Comp
L my-scale-rescue:LED-Device D1
U 1 1 601496B4
P 3700 1550
F 0 "D1" V 3739 1432 50  0000 R CNN
F 1 "heartbeat" V 3648 1432 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3700 1550 50  0001 C CNN
F 3 "~" H 3700 1550 50  0001 C CNN
	1    3700 1550
	0    -1   -1   0   
$EndComp
$Comp
L my-scale-rescue:R-Device R3
U 1 1 6014BF42
P 3700 1950
F 0 "R3" H 3770 1996 50  0000 L CNN
F 1 "330" H 3770 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 3630 1950 50  0001 C CNN
F 3 "~" H 3700 1950 50  0001 C CNN
	1    3700 1950
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR04
U 1 1 6014D370
P 3700 1300
F 0 "#PWR04" H 3700 1150 50  0001 C CNN
F 1 "+3.3V" H 3715 1473 50  0000 C CNN
F 2 "" H 3700 1300 50  0001 C CNN
F 3 "" H 3700 1300 50  0001 C CNN
	1    3700 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1700 3700 1800
Wire Notes Line width 12
	5200 1900 7600 1900
Wire Notes Line width 12
	7600 1900 7600 3650
Wire Notes Line width 12
	7650 3650 5200 3650
Wire Notes Line width 12
	5200 3650 5200 1900
Text Notes 7000 2150 2    98   Italic 20
Barcode Reader
Wire Notes Line
	750  5500 650  5500
Wire Notes Line width 12
	650  7750 5150 7750
Wire Notes Line width 12
	5150 7750 5150 5500
Wire Notes Line width 12
	5150 5500 650  5500
Wire Notes Line width 12
	650  5450 650  7750
Text Notes 3250 5800 2    98   Italic 20
Power Barcode
Connection ~ 4300 6150
Wire Wire Line
	3650 5900 3650 6150
Wire Wire Line
	3650 6150 3800 6150
$Comp
L my-scale-rescue:Conn_02x01-Connector_Generic CON4
U 1 1 601DCAB3
P 8950 4150
F 0 "CON4" H 9000 4367 50  0000 C CNN
F 1 "WP" H 9000 4276 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x01_P2.54mm_Vertical" H 8950 4150 50  0001 C CNN
F 3 "~" H 8950 4150 50  0001 C CNN
	1    8950 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 3800 9300 4150
Wire Wire Line
	9300 4150 9250 4150
Wire Wire Line
	9000 3800 9300 3800
Wire Wire Line
	8750 4150 8600 4150
$Comp
L my-scale-rescue:R-Device R5
U 1 1 601F1C23
P 9750 3950
F 0 "R5" H 9820 3996 50  0000 L CNN
F 1 "330" H 9820 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 9680 3950 50  0001 C CNN
F 3 "~" H 9750 3950 50  0001 C CNN
	1    9750 3950
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR017
U 1 1 60207CA3
P 8600 3250
F 0 "#PWR017" H 8600 3100 50  0001 C CNN
F 1 "+3.3V" H 8615 3423 50  0000 C CNN
F 2 "" H 8600 3250 50  0001 C CNN
F 3 "" H 8600 3250 50  0001 C CNN
	1    8600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3250 8600 3400
Wire Wire Line
	8600 3250 9750 3250
Wire Wire Line
	9750 3250 9750 3800
Connection ~ 8600 3250
Wire Wire Line
	9750 4100 9750 4150
Wire Wire Line
	9750 4150 9300 4150
Connection ~ 9300 4150
Wire Notes Line width 12
	7950 4600 10100 4600
Wire Notes Line width 12
	10100 4600 10100 3000
Wire Notes Line width 12
	10100 3000 7950 3000
Wire Notes Line width 12
	7950 3000 7950 4600
Text Notes 9950 4500 2    98   Italic 20
ID EEPROM HAT
Wire Wire Line
	1650 3000 1750 3000
Wire Wire Line
	1650 3100 1750 3100
Wire Wire Line
	1650 3200 1750 3200
Wire Wire Line
	1650 3300 1750 3300
Text GLabel 2450 4800 2    50   Input ~ 0
SPDIF-OUT
$Comp
L my-scale-rescue:Conn_01x06-Connector_Generic J6
U 1 1 6027B23B
P 10100 5600
F 0 "J6" H 10180 5592 50  0000 L CNN
F 1 "Finger_Print" H 10180 5501 50  0000 L CNN
F 2 "Connector_JST:JST_SH_BM06B-SRSS-TB_1x06-1MP_P1.00mm_Vertical" H 10100 5600 50  0001 C CNN
F 3 "~" H 10100 5600 50  0001 C CNN
	1    10100 5600
	1    0    0    -1  
$EndComp
Text GLabel 1600 4700 0    50   BiDi Italic 0
PA7-INT7
Text GLabel 1600 4800 0    50   BiDi Italic 0
PA8-INT8
Text GLabel 1600 4900 0    50   BiDi Italic 0
PA9-INT9
Text GLabel 1600 5000 0    50   BiDi Italic 0
PA10-INT10
$Comp
L my-scale-rescue:GND-power #PWR020
U 1 1 60282CE2
P 2600 4900
F 0 "#PWR020" H 2600 4650 50  0001 C CNN
F 1 "GND" V 2605 4772 50  0000 R CNN
F 2 "" H 2600 4900 50  0001 C CNN
F 3 "" H 2600 4900 50  0001 C CNN
	1    2600 4900
	0    -1   1    0   
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR021
U 1 1 60282CEC
P 2600 5000
F 0 "#PWR021" H 2600 4850 50  0001 C CNN
F 1 "+3.3V" V 2615 5128 50  0000 L CNN
F 2 "" H 2600 5000 50  0001 C CNN
F 3 "" H 2600 5000 50  0001 C CNN
	1    2600 5000
	0    1    -1   0   
$EndComp
Wire Wire Line
	1600 4700 1800 4700
Wire Wire Line
	1600 4800 1800 4800
Wire Wire Line
	1600 4900 1800 4900
Wire Wire Line
	1600 5000 1800 5000
Wire Wire Line
	8450 1050 8750 1050
Text GLabel 6150 4350 0    50   Output ~ 0
SPI_MOSI
Text GLabel 6150 4450 0    50   Input ~ 0
SPI_MISO
Text GLabel 6150 4550 0    50   Output ~ 0
SPI_CLK
Wire Wire Line
	2600 4900 2300 4900
Wire Wire Line
	2600 5000 2300 5000
$Comp
L my-scale-rescue:GND-power #PWR023
U 1 1 602DDAB9
P 5850 4150
F 0 "#PWR023" H 5850 3900 50  0001 C CNN
F 1 "GND" V 5855 4022 50  0000 R CNN
F 2 "" H 5850 4150 50  0001 C CNN
F 3 "" H 5850 4150 50  0001 C CNN
	1    5850 4150
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR024
U 1 1 602DDAC3
P 5850 4250
F 0 "#PWR024" H 5850 4100 50  0001 C CNN
F 1 "+3.3V" V 5865 4378 50  0000 L CNN
F 2 "" H 5850 4250 50  0001 C CNN
F 3 "" H 5850 4250 50  0001 C CNN
	1    5850 4250
	0    -1   -1   0   
$EndComp
Text GLabel 6150 4650 0    50   Output ~ 0
SPI_CE0
$Comp
L my-scale-rescue:Conn_02x01-Connector_Generic CON6
U 1 1 602EBD45
P 5700 4950
F 0 "CON6" V 5704 5030 50  0000 L CNN
F 1 "BL" V 5795 5030 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x01_P2.54mm_Vertical" H 5700 4950 50  0001 C CNN
F 3 "~" H 5700 4950 50  0001 C CNN
	1    5700 4950
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR022
U 1 1 602FAF4E
P 5700 5350
F 0 "#PWR022" H 5700 5100 50  0001 C CNN
F 1 "GND" V 5705 5222 50  0000 R CNN
F 2 "" H 5700 5350 50  0001 C CNN
F 3 "" H 5700 5350 50  0001 C CNN
	1    5700 5350
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:Conn_01x07-Connector_Generic J7
U 1 1 6030395D
P 6450 4450
F 0 "J7" H 6530 4492 50  0000 L CNN
F 1 "OLED_SPI" H 6530 4401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 6450 4450 50  0001 C CNN
F 3 "~" H 6450 4450 50  0001 C CNN
	1    6450 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4250 6250 4250
Wire Wire Line
	6150 4350 6250 4350
Wire Wire Line
	6150 4450 6250 4450
Wire Wire Line
	6150 4550 6250 4550
Wire Wire Line
	6150 4650 6250 4650
Wire Wire Line
	5700 4750 6250 4750
Wire Wire Line
	5700 5250 5700 5350
Text GLabel 8000 5450 0    50   Output ~ 0
SPI_MOSI
Text GLabel 8000 5550 0    50   Input ~ 0
SPI_MISO
Text GLabel 8000 5650 0    50   Output ~ 0
SPI_CLK
$Comp
L my-scale-rescue:GND-power #PWR025
U 1 1 6033FD93
P 7700 5250
F 0 "#PWR025" H 7700 5000 50  0001 C CNN
F 1 "GND" V 7705 5122 50  0000 R CNN
F 2 "" H 7700 5250 50  0001 C CNN
F 3 "" H 7700 5250 50  0001 C CNN
	1    7700 5250
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR026
U 1 1 6033FD9D
P 7700 5350
F 0 "#PWR026" H 7700 5200 50  0001 C CNN
F 1 "+3.3V" V 7715 5478 50  0000 L CNN
F 2 "" H 7700 5350 50  0001 C CNN
F 3 "" H 7700 5350 50  0001 C CNN
	1    7700 5350
	0    -1   -1   0   
$EndComp
Text GLabel 8000 5750 0    50   Output ~ 0
SPI_CE1
Wire Wire Line
	7700 5250 8100 5250
Wire Wire Line
	7700 5350 8100 5350
Wire Wire Line
	8000 5450 8100 5450
Wire Wire Line
	8000 5550 8100 5550
Wire Wire Line
	8000 5650 8100 5650
$Comp
L my-scale-rescue:Conn_01x08-Connector_Generic J8
U 1 1 6036A9C2
P 8300 5550
F 0 "J8" H 8380 5542 50  0000 L CNN
F 1 "RFID_SPI" H 8380 5451 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 8300 5550 50  0001 C CNN
F 3 "~" H 8300 5550 50  0001 C CNN
	1    8300 5550
	1    0    0    -1  
$EndComp
Text GLabel 2900 2600 2    50   Input ~ 0
RFID_INT
Wire Wire Line
	2250 2600 2900 2600
Text GLabel 8000 5950 0    50   Output ~ 0
RFID_INT
Text GLabel 8000 5850 0    50   Input ~ 0
RFID_PWR
Text GLabel 2900 2400 2    50   Output ~ 0
RFID_PWR
Wire Wire Line
	2250 2400 2900 2400
Wire Wire Line
	8000 5850 8100 5850
Wire Wire Line
	8000 5950 8100 5950
Wire Wire Line
	6600 2450 6700 2450
Wire Wire Line
	1550 2400 1750 2400
Wire Wire Line
	8100 5750 8000 5750
Wire Wire Line
	6250 4150 5850 4150
Wire Wire Line
	950  5750 1050 5750
Wire Wire Line
	1050 5750 1050 5700
Connection ~ 1050 5750
Wire Wire Line
	1050 5750 1250 5750
Wire Wire Line
	3700 1400 3700 1300
Wire Wire Line
	8350 1150 8750 1150
Text GLabel 9800 5600 0    50   Output ~ 0
UART3-RX
Text GLabel 9800 5700 0    50   Input ~ 0
UART3-TX
Wire Wire Line
	9800 5700 9900 5700
Wire Wire Line
	9800 5600 9900 5600
$Comp
L my-scale-rescue:GND-power #PWR028
U 1 1 604A932D
P 9650 5500
F 0 "#PWR028" H 9650 5250 50  0001 C CNN
F 1 "GND" V 9655 5372 50  0000 R CNN
F 2 "" H 9650 5500 50  0001 C CNN
F 3 "" H 9650 5500 50  0001 C CNN
	1    9650 5500
	0    1    1    0   
$EndComp
$Comp
L my-scale-rescue:+3.3V-power #PWR027
U 1 1 604A9337
P 9650 5400
F 0 "#PWR027" H 9650 5250 50  0001 C CNN
F 1 "+3.3V" V 9665 5528 50  0000 L CNN
F 2 "" H 9650 5400 50  0001 C CNN
F 3 "" H 9650 5400 50  0001 C CNN
	1    9650 5400
	0    -1   -1   0   
$EndComp
$Comp
L my-scale-rescue:+5V-power #PWR029
U 1 1 604C4305
P 9650 5900
F 0 "#PWR029" H 9650 5750 50  0001 C CNN
F 1 "+5V" V 9665 6028 50  0000 L CNN
F 2 "" H 9650 5900 50  0001 C CNN
F 3 "" H 9650 5900 50  0001 C CNN
	1    9650 5900
	0    -1   1    0   
$EndComp
Text GLabel 2900 2300 2    50   Output ~ 0
FPM-WAKE
Text GLabel 9800 5800 0    50   Output ~ 0
FPM-WAKE
Wire Wire Line
	9650 5500 9900 5500
Wire Wire Line
	9800 5800 9900 5800
Wire Wire Line
	9650 5900 9900 5900
Text Label 2750 2300 2    50   ~ 0
PA15_ENT15
Text Label 2700 3300 2    50   ~ 0
ENT4_PL4
Text Label 2650 3100 2    50   ~ 0
ENT2_PL2
Text Label 2650 2600 2    50   ~ 0
ENT2_PA2
Text Label 2450 2400 2    50   ~ 0
PC4
Text Label 2750 2100 2    50   ~ 0
PA16_ENT16
Text Label 1700 2300 2    50   ~ 0
PA3_ENT3
Text Label 1700 2900 2    50   ~ 0
PA19_ENT19
Text Label 2750 2900 2    50   ~ 0
PA18_ENT18
Wire Wire Line
	4050 3850 4050 3950
Wire Wire Line
	4050 4250 4050 4350
Wire Wire Line
	4050 4350 4100 4350
Text GLabel 2900 3100 2    50   Output ~ 0
Barcode-PWR
Text GLabel 1400 1900 0    50   Output ~ 0
PWM1
Wire Wire Line
	1400 1900 1750 1900
Text Label 1600 1900 2    50   ~ 0
PA6
Wire Notes Line width 12
	10800 5050 10800 6200
Wire Notes Line width 12
	10800 6200 9100 6200
Wire Notes Line width 12
	9100 6200 9100 5050
Wire Notes Line width 12
	9100 5050 10800 5050
Text Notes 10250 5250 2    100  Italic 20
FingerPrint
Wire Wire Line
	9650 5400 9900 5400
Wire Notes Line width 12
	8900 6200 7150 6200
Wire Notes Line width 12
	7150 6200 7150 4850
Wire Notes Line width 12
	7150 4850 8900 4850
Wire Notes Line width 12
	8900 4850 8900 6200
Text Notes 8250 5100 2    100  Italic 20
RFID SPI
$Comp
L my-scale-rescue:+5V-power #PWR019
U 1 1 6079C34C
P 2650 4700
F 0 "#PWR019" H 2650 4550 50  0001 C CNN
F 1 "+5V" V 2665 4828 50  0000 L CNN
F 2 "" H 2650 4700 50  0001 C CNN
F 3 "" H 2650 4700 50  0001 C CNN
	1    2650 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 4700 2650 4700
$Comp
L my-scale-rescue:Conn_02x04_Odd_Even-Connector_Generic J9
U 1 1 607BA18F
P 2000 4800
F 0 "J9" H 2050 5117 50  0000 C CNN
F 1 "Expansion" H 2050 5026 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 2000 4800 50  0001 C CNN
F 3 "~" H 2000 4800 50  0001 C CNN
	1    2000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 4800 2300 4800
Wire Notes Line width 12
	800  4200 3200 4200
Wire Notes Line width 12
	3200 4200 3200 5200
Wire Notes Line width 12
	3200 5200 800  5200
Wire Notes Line width 12
	800  4200 800  5200
Text Notes 1850 4400 2    100  Italic 20
Expansion
Wire Notes Line width 12
	7100 5700 5250 5700
Wire Notes Line width 12
	5250 5700 5250 3800
Wire Notes Line width 12
	7100 3800 7100 5700
Wire Notes Line width 12
	5250 3800 7100 3800
Text Notes 6400 4000 2    100  Italic 20
OLED SPI
Wire Wire Line
	2250 2100 3700 2100
$Comp
L my-scale-rescue:MountingHole_Pad-Mechanical H2
U 1 1 60043954
P 10450 1400
F 0 "H2" H 10400 1650 50  0000 L CNN
F 1 "h" H 10150 1750 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_TopBottom" H 10450 1400 50  0001 C CNN
F 3 "~" H 10450 1400 50  0001 C CNN
	1    10450 1400
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:MountingHole_Pad-Mechanical H1
U 1 1 60044913
P 10000 1200
F 0 "H1" H 9950 1400 50  0000 L CNN
F 1 "h" H 9700 1500 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_Pad_TopBottom" H 10000 1200 50  0001 C CNN
F 3 "~" H 10000 1200 50  0001 C CNN
	1    10000 1200
	1    0    0    -1  
$EndComp
$Comp
L my-scale-rescue:GND-power #PWR030
U 1 1 60045214
P 10100 1600
F 0 "#PWR030" H 10100 1350 50  0001 C CNN
F 1 "GND" V 10105 1472 50  0000 R CNN
F 2 "" H 10100 1600 50  0001 C CNN
F 3 "" H 10100 1600 50  0001 C CNN
	1    10100 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 1300 10000 1600
Wire Wire Line
	10000 1600 10100 1600
Wire Wire Line
	10450 1500 10250 1600
Wire Wire Line
	10250 1600 10100 1600
Connection ~ 10100 1600
Text Label 3150 6800 0    50   ~ 0
FB
Text Label 3150 6600 0    50   ~ 0
SW
Text Label 3000 6400 0    50   ~ 0
BST
Text Label 1500 6800 0    50   ~ 0
PGOOD
Text Label 9450 4150 0    50   ~ 0
WP
Wire Wire Line
	2250 2300 2900 2300
$EndSCHEMATC
